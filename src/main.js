import Vue from 'vue'
import StoryblokVue from 'storyblok-vue'
import VueAnalytics from 'vue-analytics'
import underscore from 'underscore'

import './plugins/vuetify'
import App from './App.vue'
import Loading from './libs/Loading.js'
import router from './router'

Vue.config.productionTip = false
const isProd = process.env.NODE_ENV === 'production'

Vue.prototype.$_ = underscore
Vue.prototype.$loading = Loading.image();

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')

Vue.use(StoryblokVue)
Vue.use(VueAnalytics, {
    id: 'UA-139190314-1',
    router,
    debug: {
        enabled: !isProd,
        sendHitTask: isProd
    }
})
