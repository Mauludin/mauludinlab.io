import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import Maintenance from './views/Maintenance.vue'
import Error from './views/Error.vue'

import Home from './views/Home.vue'
import Contact from './views/Contact.vue'
import Resume from './views/Resume.vue'
import Portfolio from './views/Portfolio.vue'
import Certificate from './views/Certificate.vue'

import PortfolioWebsite from './views/Portfolio/Website.vue'
import PortfolioAndroid from './views/Portfolio/Android.vue'

import CertificateSoftSkills from './views/Certificate/Softskills.vue'
import CertificateCompetence from './views/Certificate/Competence.vue'
import CertificateResearch from './views/Certificate/Research.vue'
import CertificateAchievement from './views/Certificate/Achievement.vue'

Vue.use(Router)
Vue.use(Meta)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        // {
        //   path: '/',
        //   name: 'home',
        //   redirect: { name: 'maintenance' }
        // },
        {
            path: '/',
            name: 'home',
            component: Home,
            props: true
        },
        // {
        //     path: '/maintenance',
        //     name: 'maintenance',
        //     component: Maintenance
        // },
        {
            path: '/contact',
            name: 'contact',
            component: Contact
        },
        {
            path: '/resume',
            name: 'resume',
            component: Resume
        },
        {
            path: '/portfolio',
            name: 'portfolio',
            component: Portfolio
        },
        {
            path: '/portfolio/website',
            name: 'portfolio-website',
            component: PortfolioWebsite
        },
        {
            path: '/portfolio/android',
            name: 'portfolio-android',
            component: PortfolioAndroid
        },
        {
            path: '/certificate',
            name: 'certificate',
            component: Certificate
        },
        {
            path: '/certificate/softskills',
            name: 'certificate-softskills',
            component: CertificateSoftSkills
        },
        {
            path: '/certificate/competence',
            name: 'certificate-competence',
            component: CertificateCompetence
        },
        {
            path: '/certificate/research',
            name: 'certificate-research',
            component: CertificateResearch
        },
        {
            path: '/certificate/achievement',
            name: 'certificate-achievement',
            component: CertificateAchievement
        },
        {
            path: '*',
            name: 'Error',
            component: Error
        }
    ]
})
